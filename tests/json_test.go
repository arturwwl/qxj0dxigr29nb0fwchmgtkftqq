package tests

import (
	"encoding/json"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/models"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/services"
	"testing"
)

func TestJsonDecode(t *testing.T) {
	jsonString := `{"copyright":"Amir H. Abolfath","date":"2019-12-06","explanation":"This cosmic vista stretches almost 20 degrees from top to bottom, across the dusty constellation Taurus. It begins at the Pleiades and ends at the Hyades, two star clusters recognized since antiquity in Earth's night sky. At top, the compact Pleiades star cluster is about 400 light-years away. The lovely grouping of young cluster stars shine through dusty clouds that scatter blue starlight. At bottom, the V-shaped Hyades cluster looks more spread out in comparison and lies much closer, 150 light-years away. The Hyades cluster stars seem anchored by bright Aldebaran, a red giant star with a yellowish appearance. But Aldebaran actually lies only 65 light-years distant and just by chance along the line of sight to the Hyades cluster. Faint and darkly obscuring dust clouds found near the edge of the Taurus Molecular Cloud are also evident throughout the celestial scene. The wide field of view includes the dark nebula Barnard 22 at left with youthful star T Tauri and Hind's variable nebula just above Aldebaran in the frame.","hdurl":"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg","media_type":"image","service_version":"v1","title":"Pleiades to Hyades","url":"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath1024.jpg"}`

	apod := models.Apod{}

	err := json.Unmarshal([]byte(jsonString), &apod)
	if err != nil {
		t.Fatal(err)
	}

	if apod.Title == "" {
		t.Error("title could not be parsed")
	} else if apod.Title != "Pleiades to Hyades" {
		t.Error("title could not be parsed correctly")
	}

	if apod.Url == "" {
		t.Error("url could not be parsed")
	} else if apod.Url != "https://apod.nasa.gov/apod/image/1912/TaurusAbolfath1024.jpg" {
		t.Error("url could not be parsed correctly")
	}

	if apod.Date == "" {
		t.Error("date could not be parsed")
	} else if apod.Date != "2019-12-06" {
		t.Error("date could not be parsed correctly")
	}

	if apod.Copyright == "" {
		t.Error("copyright could not be parsed")
	} else if apod.Copyright != "Amir H. Abolfath" {
		t.Error("copyright could not be parsed correctly")
	}

	if apod.Explanation == "" {
		t.Error("explanation could not be parsed")
	} else if apod.Explanation != `This cosmic vista stretches almost 20 degrees from top to bottom, across the dusty constellation Taurus. It begins at the Pleiades and ends at the Hyades, two star clusters recognized since antiquity in Earth's night sky. At top, the compact Pleiades star cluster is about 400 light-years away. The lovely grouping of young cluster stars shine through dusty clouds that scatter blue starlight. At bottom, the V-shaped Hyades cluster looks more spread out in comparison and lies much closer, 150 light-years away. The Hyades cluster stars seem anchored by bright Aldebaran, a red giant star with a yellowish appearance. But Aldebaran actually lies only 65 light-years distant and just by chance along the line of sight to the Hyades cluster. Faint and darkly obscuring dust clouds found near the edge of the Taurus Molecular Cloud are also evident throughout the celestial scene. The wide field of view includes the dark nebula Barnard 22 at left with youthful star T Tauri and Hind's variable nebula just above Aldebaran in the frame.` {
		t.Error("explanation could not be parsed correctly")
	}

	if apod.HDUrl == "" {
		t.Error("hd url could not be parsed")
	} else if apod.HDUrl != "https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg" {
		t.Error("hd url could not be parsed correctly")
	}

	if apod.MediaType == "" {
		t.Error("media type could not be parsed")
	} else if apod.MediaType != "image" {
		t.Error("media type could not be parsed correctly")
	}

	if apod.ServiceVersion == "" {
		t.Error("service version could not be parsed")
	} else if apod.ServiceVersion != "v1" {
		t.Error("service version could not be parsed correctly")
	}
}

func TestJsonEncode(t *testing.T) {
	a := models.Apod{
		Copyright:      "Balint Lengyel",
		Date:           "2021-08-20",
		Explanation:    "Frames from a camera that spent three moonless nights under the stars create this composite night skyscape. They were recorded during August 11-13 while planet Earth was sweeping through the dusty trail of comet Swift-Tuttle. One long exposure, untracked for the foreground, and the many star tracking captures of Perseid shower meteors were taken from the village of Magyaregres, Hungary. Each aligned against the background stars, the meteor trails all point back to the annual shower's radiant in the constellation Perseus heroically standing above this rural horizon. Of course the comet dust particles are traveling along trajectories parallel to each other. The radiant effect is due only to perspective, as the parallel tracks appear to converge in the distance against the starry sky.   Notable APOD Image Submissions: Perseid Meteor Shower 2021",
		HDUrl:          "https://apod.nasa.gov/apod/image/2108/ThreeNightsPerseids3000.jpg",
		MediaType:      "image",
		ServiceVersion: "v1",
		Title:          "Three Perseid Nights",
		Url:            "https://apod.nasa.gov/apod/image/2108/ThreeNightsPerseids1024.jpg",
	}

	if a.GetUrl() != "https://apod.nasa.gov/apod/image/2108/ThreeNightsPerseids1024.jpg" {
		t.Error("malformed get url")
	}

	bytes, err := json.Marshal(a)
	if err != nil {
		t.Error(err)
	}

	if string(bytes) != `{"copyright":"Balint Lengyel","date":"2021-08-20","explanation":"Frames from a camera that spent three moonless nights under the stars create this composite night skyscape. They were recorded during August 11-13 while planet Earth was sweeping through the dusty trail of comet Swift-Tuttle. One long exposure, untracked for the foreground, and the many star tracking captures of Perseid shower meteors were taken from the village of Magyaregres, Hungary. Each aligned against the background stars, the meteor trails all point back to the annual shower's radiant in the constellation Perseus heroically standing above this rural horizon. Of course the comet dust particles are traveling along trajectories parallel to each other. The radiant effect is due only to perspective, as the parallel tracks appear to converge in the distance against the starry sky.   Notable APOD Image Submissions: Perseid Meteor Shower 2021","hdurl":"https://apod.nasa.gov/apod/image/2108/ThreeNightsPerseids3000.jpg","media_type":"image","service_version":"v1","title":"Three Perseid Nights","url":"https://apod.nasa.gov/apod/image/2108/ThreeNightsPerseids1024.jpg"}` {
		t.Error("not parsed correctly")
	}
}

func TestParseApodData(t *testing.T) {
	ApodData := []models.Apod{
		{
			Copyright:      "Balint Lengyel",
			Date:           "2021-08-20",
			Explanation:    "Frames from a camera that spent three moonless nights under the stars create this composite night skyscape. They were recorded during August 11-13 while planet Earth was sweeping through the dusty trail of comet Swift-Tuttle. One long exposure, untracked for the foreground, and the many star tracking captures of Perseid shower meteors were taken from the village of Magyaregres, Hungary. Each aligned against the background stars, the meteor trails all point back to the annual shower's radiant in the constellation Perseus heroically standing above this rural horizon. Of course the comet dust particles are traveling along trajectories parallel to each other. The radiant effect is due only to perspective, as the parallel tracks appear to converge in the distance against the starry sky.   Notable APOD Image Submissions: Perseid Meteor Shower 2021",
			HDUrl:          "https://apod.nasa.gov/apod/image/2108/ThreeNightsPerseids3000.jpg",
			MediaType:      "image",
			ServiceVersion: "v1",
			Title:          "Three Perseid Nights",
			Url:            "https://apod.nasa.gov/apod/image/2108/ThreeNightsPerseids1024.jpg",
		},
		{
			Copyright:      "Christopher Go",
			Date:           "2021-08-21",
			Explanation:    "These three panels feature the Solar System's ruling gas giant Jupiter on August 15 as seen from Cebu City, Phillipines, planet Earth. On that date the well-timed telescopic views detail some remarkable performances, transits and mutual events, by Jupiter's Galilean moons. In the top panel, Io is just disappearing into Jupiter's shadow at the far right, but the three other large Jovian moons appear against the planet's banded disk. Brighter Europa and darker Ganymede are at the far left, also casting their two shadows on the gas giant's cloud tops. Callisto is below and right near the planet's edge, the three moons in a triple transit across the face of Jupiter. Moving to the middle panel, shadows of Europa and Ganymede are still visible near center but Ganymede has occulted or passed in front of Europa. The bottom panel captures a rare view of Jovian moons in eclipse while transiting Jupiter, Ganymede's shadow falling on Europa itself. From planet Earth's perspective, similar mutual events, when Galilean moons occult and eclipse each other, can be seen every six years or so when Jupiter is near its own equinox.",
			HDUrl:          "https://apod.nasa.gov/apod/image/2108/j20210815a_cgo.jpg",
			MediaType:      "image",
			ServiceVersion: "v1",
			Title:          "Triple Transit and Mutual Events",
			Url:            "https://apod.nasa.gov/apod/image/2108/j20210815a_cgo_crop1200.jpg",
		},
	}
	pictures := services.ParseApod(ApodData)
	if len(pictures.Urls) != 2 {
		t.Error("urls len must be equal to 2")
	}

	for _, u := range pictures.Urls {
		if u == "" {
			t.Error("url must not be empty")
		}
	}

	bytes, err := json.Marshal(pictures)
	if err != nil {
		t.Error(err)
	}

	if string(bytes) != `{"urls":["https://apod.nasa.gov/apod/image/2108/ThreeNightsPerseids1024.jpg","https://apod.nasa.gov/apod/image/2108/j20210815a_cgo_crop1200.jpg"]}` {
		t.Error("json output not as expected")
	}
}
