package main

import (
	"fmt"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/routers"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/services"
	"net/http"
)

func main() {
	r := routers.NewMainRouter()

	err := http.ListenAndServe(fmt.Sprintf("0.0.0.0:%d", services.GetIntEnvVar("PORT", 8080)), r)
	panic(err)
}
