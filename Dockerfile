# Cannot be alipne, because it does not have git
FROM golang:1.15

WORKDIR /app

# Download necessary Go modules
COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .

RUN go build -o /url-collector

EXPOSE 8080

CMD [ "/url-collector" ]
