package services

import (
	"encoding/json"
	"fmt"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/models"
	"log"
	"net/url"
	"time"
)

const baseUrl = "https://api.nasa.gov/planetary/apod?api_key=%s"

var maxConcurrentRequests int
var maxConcurrentRequestsChan chan struct{}

func init() {
	maxConcurrentRequests = GetIntEnvVar("CONCURRENT_REQUESTS", 5)
	maxConcurrentRequestsChan = make(chan struct{}, maxConcurrentRequests)
}

func getBaseApodUrl() string {
	return fmt.Sprintf(baseUrl, GetEnvVar("API_KEY", "DEMO_KEY"))
}

var requestCounter int

func GetApodPictures(startDate *time.Time, endDate *time.Time) (pictures []models.Apod) {
	pictures = make([]models.Apod, 0)
	requestCounter++
	log.Println(requestCounter)
	parsedUrl, err := url.Parse(getBaseApodUrl())
	if err != nil {
		panic(err)
	}

	if q := parsedUrl.Query(); q != nil {

		if startDate != nil {
			q.Set("start_date", startDate.Format("2006-01-02"))
		}
		if endDate != nil {
			q.Set("end_date", endDate.Format("2006-01-02"))
		}

		//only pictures needed, so instead of youtube link get thumbs
		q.Set("thumbs", "true")

		// update query parameters
		parsedUrl.RawQuery = q.Encode()
	}

	maxConcurrentRequestsChan <- struct{}{}
	defer func() { <-maxConcurrentRequestsChan }()
	bytes, err := makeRequest("GET", parsedUrl.String(), nil)
	if err != nil {
		panic(err)
	}

	err = json.Unmarshal(bytes, &pictures)
	if err != nil {
		panic(err)
	}

	return
}
