package services

import "gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/models"

func ParseApod(pictures []models.Apod) (r models.Pictures) {
	for _, pic := range pictures {
		r.Urls = append(r.Urls, parseSinglePicture(pic))
	}
	return
}

func parseSinglePicture(picture models.Picture) string {
	return picture.GetUrl()
}
