package services

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
)

func validateMethod(method string) (err error) {
	switch method {
	// base valid methods, others not needed
	case "GET", "POST", "PUT", "PATCH", "HEAD", "DELETE":
	default:
		err = fmt.Errorf("invalid method - %s", method)
	}

	return
}

func makeRequest(method string, url string, data interface{}) ([]byte, error) {
	var requestBody io.Reader
	var err error
	var req *http.Request

	err = validateMethod(method)
	if err != nil {
		return nil, err
	}

	if data != nil && method != "GET" && method != "HEAD" {
		postBody, _ := json.Marshal(data)
		requestBody = bytes.NewBuffer(postBody)
	}

	req, err = http.NewRequest(method, url, requestBody)
	if err != nil {
		return nil, err
	}

	if data != nil {
		req.Header.Add("content-type", "application/json")
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode >= 400 {
		return nil, fmt.Errorf("invalid resposne status code")
	}

	return bodyBytes, err
}
