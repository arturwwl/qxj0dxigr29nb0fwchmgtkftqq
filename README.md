# Url Collector

A microservice that would prepare a list of urls for the `media-downloader`

## One endpoint 

### Get Pictures
`/pictures?start_date=${startDate}&end_date=${endDate}`

#### example success response
```json
{
  "urls": [
    "https://apod.nasa.gov/apod/image/2108/ThreeNightsPerseids1024.jpg",
    "https://apod.nasa.gov/apod/image/2108/j20210815a_cgo_crop1200.jpg",
    "https://apod.nasa.gov/apod/image/2108/rsoph_pparc_960.jpg"
  ]
}

```
#### example error response
```json
{
  "error": "end date cannot be in future"
}
```

## Discuss
1. What if we were to change the NASA API to some other images provider?
  - A: Then will be needed to update model, api service and parser
2. What if, apart from using NASA API, we would want to have another microservice fetching urls from European Space Agency. How much code could be reused?
  - A: Most of code
3. What if we wanted to add some more query params to narrow down lists of urls - for example, selecting only images taken by certain person. (field copyright in the API response)
  - A: Field copyright could not be filtered via API(https://github.com/nasa/apod-api), but we can filter this in parser (omit those which does not match)