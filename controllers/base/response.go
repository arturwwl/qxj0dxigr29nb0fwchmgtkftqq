package base

import (
	"encoding/json"
	"fmt"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/models"
	"log"
	"net/http"
)

func NewResponse(w http.ResponseWriter) *Response {
	return &Response{
		ResponseWriter: w,
	}
}

type Response struct {
	Data           interface{}
	ResponseWriter http.ResponseWriter
	Status         int
}

func (r *Response) BadRequest(err string) {
	r.Data = models.NewError(err)
	r.Status = http.StatusBadRequest
}

func (r *Response) SendResponse() {
	// catch unexpected error
	if pi := recover(); pi != nil {
		// response status - internal server error
		r.Status = http.StatusInternalServerError
		// internal error message
		r.Data = models.Error{
			Error: "internal server error",
		}
		//get well formatted error log
		switch fmt.Sprintf("%T", pi) {
		//case "base.panicInterface":
		case "string":
			log.Println(pi.(string))
		case "error":
			log.Println(pi.(error))
		default:
			log.Println(fmt.Sprintf("%v", pi))
		}
	}

	r.writeJSONResponse(&r.Data)
}

func (r *Response) writeJSONResponse(st interface{}) {
	var err error
	r.ResponseWriter.Header().Set("Content-Type", "application/json")
	r.ResponseWriter.WriteHeader(r.Status)

	bytes, err := json.Marshal(st)
	if err != nil {
		log.Println(err)
	}

	_, err = r.ResponseWriter.Write(bytes)
	if err != nil {
		log.Println(err)
	}
}
