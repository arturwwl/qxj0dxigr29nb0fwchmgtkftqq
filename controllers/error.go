package controllers

import (
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/controllers/base"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/models"
	"net/http"
)

func NotFound(w http.ResponseWriter, r *http.Request) {
	response := base.NewResponse(w)
	defer response.SendResponse()

	response.Data = models.Error{Error: "not found"}
	response.Status = http.StatusNotFound
}
