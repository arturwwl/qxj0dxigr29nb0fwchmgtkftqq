package controllers

import (
	"github.com/gorilla/mux"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/controllers/base"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/services"
	"net/http"
	"time"
)

func GetPictures(w http.ResponseWriter, r *http.Request) {
	response := base.NewResponse(w)
	defer response.SendResponse()

	var startDate *time.Time
	var endDate *time.Time
	if vars := mux.Vars(r); len(vars) > 0 {
		for k, v := range vars {
			switch k {
			case "startDate":
				t, err := time.Parse("2006-01-02", v)
				if err != nil {
					response.BadRequest("invalid start date")
					return
				}
				startDate = &t
			case "endDate":
				t, err := time.Parse("2006-01-02", v)
				if err != nil {
					response.BadRequest("invalid end date")
					return
				}
				endDate = &t
			}
		}
	}

	if startDate != nil {
		if time.Now().Before(*startDate) {
			response.BadRequest("start date cannot be in future")
			return
		}
	}
	if endDate != nil {
		if time.Now().Before(*endDate) {
			response.BadRequest("end date cannot be in future")
			return
		}
	}
	if startDate != nil && endDate != nil {
		if startDate.After(*endDate) {
			response.BadRequest("start date cannot be after end date")
			return
		}
	}

	pictures := services.GetApodPictures(startDate, endDate)

	response.Data = services.ParseApod(pictures)
	response.Status = http.StatusOK
}
