package models

type Picture interface {
	GetUrl() string
}

type Pictures struct {
	Urls []string `json:"urls,omitempty"`
}
