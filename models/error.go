package models

func NewError(err string) Error {
	return Error{Error: err}
}

type Error struct {
	Error string `json:"error,omitempty"`
}
