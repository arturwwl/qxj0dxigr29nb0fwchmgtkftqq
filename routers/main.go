package routers

import (
	"github.com/gorilla/mux"
	"gitlab.com/arturwwl/qxj0dxigr29nb0fwchmgtkftqq/controllers"
)

func NewMainRouter() (r *mux.Router) {
	r = mux.NewRouter()

	//not found handler
	r.Path("/pictures").Queries("start_date", "{startDate}", "end_date", "{endDate}").HandlerFunc(controllers.GetPictures)
	r.NotFoundHandler = r.NewRoute().HandlerFunc(controllers.NotFound).GetHandler()

	return
}
